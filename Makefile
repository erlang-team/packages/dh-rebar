DH_REBAR_DIR=usr/share/dh-rebar/
DH_DIR=usr/share/perl5/Debian/Debhelper/

build: man/dh_rebar.1
clean:
	rm -rf man

install:
	mkdir -p $(DESTDIR)/$(DH_REBAR_DIR)/template/
	mkdir -p $(DESTDIR)/$(DH_REBAR_DIR)/make/
	mkdir -p $(DESTDIR)/$(DH_DIR)/Buildsystem/
	mkdir -p $(DESTDIR)/$(DH_DIR)/Sequence/
	mkdir -p $(DESTDIR)/usr/bin/
	cp template/* $(DESTDIR)/$(DH_REBAR_DIR)/template/
	cp make/* $(DESTDIR)/$(DH_REBAR_DIR)/make/
	cp debhelper7/Buildsystem/* $(DESTDIR)/$(DH_DIR)/Buildsystem/
	cp debhelper7/Sequence/* $(DESTDIR)/$(DH_DIR)/Sequence/
	cp bin/dh_rebar $(DESTDIR)/usr/bin/

man/dh_rebar.1: bin/dh_rebar
	@if [ ! -d man ] ; then mkdir man; fi
	pod2man -c "dh_rebar" -r "dh_rebar" bin/dh_rebar man/dh_rebar.1

