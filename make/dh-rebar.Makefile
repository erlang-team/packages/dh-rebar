# Copyright: $BB)(B 2012 Enrico Tassi <gareuselesinge@debian.org>
# Copyright: 2013 Nobuhiro iwamatsu <iwamatsu@debian.org>
# License: MIT/X

# override for verbose output
ifeq "$(DH_VERBOSE)-" "-"
H=@
else
H=
endif

include debian/dh-rebar.conf

# package name and version
# PKG_NAME: package name (e.g. foo-bar)
# DEB_PKG: Debian package name (Add erlang-. e.g. erlang-foo-bar)
# E_PKG_NAME: Erlang package name (use underscore instead of dash. e.g. foo_bar)
ifeq "$(PKG_NAME)" ""
PKG_NAME=$(shell dpkg-parsechangelog | sed -rne 's/^Source: erlang-(.*)/\1/p')
DEB_PKG=erlang-$(PKG_NAME)
else
DEB_PKG=$(PKG_NAME)
endif
E_PKG_NAME=$(shell echo $(PKG_NAME) | sed -e 's/-/_/g')
DEB_PKG_DEV=$(DEB_PKG)-dev

ifeq "$(PKG_VERSION)" ""
PKG_VERSION=$(shell dpkg-parsechangelog | sed -rne 's/^Version: ([0-9.]+)(\+dfsg)?.*$$/\1/p')
endif

ifeq "$(REBAR_INCLUDE_DIR)" ""
REBAR_INCLUDE_DIR=include
endif
ifeq "$(REBAR_LIB_DIR)" ""
REBAR_LIB_DIR=priv
endif
ifeq "$(REBAR_BIN_DIR)" ""
REBAR_BIN_DIR=ebin
endif
ifeq "$(REBAR_DEPS_PREFER_LIBS)" ""
export REBAR_DEPS_PREFER_LIBS=TRUE
endif
ifeq "$(ERL_COMPILER_OPTIONS)" ""
export ERL_COMPILER_OPTIONS=deterministic
endif

# CFLAGS
CFLAGS := $(shell dpkg-buildflags --get CFLAGS)
CFLAGS += $(shell dpkg-buildflags --get CPPFLAGS)
STD_LDFLAGS := $(shell dpkg-buildflags --get LDFLAGS)
LDFLAGS = $(STD_LDFLAGS) $(CLIB_LDFLAGS)

# .install
DH_REBAR_SUPPORT_FILES=/usr/share/dh-rebar/
DEB_INST_PKG=debian/$(DEB_PKG).install
DEB_INST_PKG_TEMPL=$(DH_REBAR_SUPPORT_FILES)template/pkg.install.in
DEB_INST_DEV=debian/$(DEB_PKG_DEV).install
DEB_INST_DEV_TEMPL=$(DH_REBAR_SUPPORT_FILES)template/dev.install.in

# macros
define subst_vars
	sed \
		-e 's/@@PKG_VERSION@@/$(PKG_VERSION)/g' \
		-e 's/@@E_PKG_NAME@@/$(E_PKG_NAME)/g' \
		-e 's/@@REBAR_LIB_DIR@@/$(REBAR_LIB_DIR)/g' \
		-e 's/@@REBAR_BIN_DIR@@/$(REBAR_BIN_DIR)/g' \
		-e 's/@@REBAR_INCLUDE_DIR@@/$(REBAR_INCLUDE_DIR)/g' \
		-e '/^test /e'
endef

define trash
	echo $(1) >> debian/trash
endef

define empty_trash
	if [ -e debian/trash ]; then \
		$(RM) -f `cat debian/trash`; fi
endef

define merge_with
	(while read line; do\
		[ -e $(1) ] || touch $(1);\
		if grep -q -F "$$line" $(1); then\
			echo "Skipping already existing line: $$line";\
		else\
			echo "Adding new line: $$line";\
			echo "$$line" >> $(1);\
		fi;\
	done)
endef


.PHONY: build clean test install configure

clean_debhelper:
	$(H)$(call empty_trash)
	$(H)$(RM) -rf debian/trash

# build
ifneq (,$(findstring compile, $(EXEC_REBAR_COMMANDS)))
BUILD_TARGET=rebar_compile
endif
ifneq (,$(findstring xref, $(EXEC_REBAR_COMMANDS)))
BUILD_TARGET+=rebar_xref
endif
ifneq (,$(findstring doc, $(EXEC_REBAR_COMMANDS)))
BUILD_TARGET+=rebar_doc
endif
ifneq (,$(findstring ct, $(EXEC_REBAR_COMMANDS)))
BUILD_TARGET+=rebar_ct
endif

build: $(BUILD_TARGET)

# test
ifneq (,$(findstring eunit, $(EXEC_REBAR_COMMANDS)))
TEST_TARGET=rebar_eunit
endif

test: $(TEST_TARGET)

configure:

clean: clean_debhelper
	$(H)echo $@
	rebar clean skip_deps=true -vv
	$(H)rm -rf ebin/*.beam
	$(H)rm -rf ebin/*.app
	$(H)rm -rf c_src/*.o
	$(H)rm -rf priv/.so
	$(H)rm -rf .eunit

rebar_compile:
	$(H)echo $@
	rebar compile skip_deps=true -vv

rebar_xref:
	$(H)echo $@
	rebar xref skip_deps=true -vv

rebar_doc:
	$(H)echo $@
	rebar doc skip_deps=true -vv

rebar_eunit:
	$(H)@echo $@
	rebar eunit skip_deps=true -vv

rebar_ct:
	$(H)@echo $@
	rebar ct skip_deps=true -vv

install:
	$(H)echo $@
	$(H)$(call trash, $(DEB_INST_PKG))
	$(H)if [ -e $(DEB_INST_PKG).in ]; then\
		echo "Filling in $(DEB_INST_PKG) using $(DEB_INST_PKG).in";\
		cat $(DEB_INST_PKG).in | $(call subst_vars) | \
			$(call merge_with, $(DEB_INST_PKG));\
	else \
		echo "Filling in $(DEB_INST_PKG) using $(DEB_INST_PKG_TEMPL)";\
		cat $(DEB_INST_PKG_TEMPL) | $(call subst_vars) | \
			$(call merge_with, $(DEB_INST_PKG));\
	fi
	$(H)$(call trash, $(DEB_INST_DEV))
	$(H)if [ -e $(DEB_INST_DEV).in ]; then\
		echo "Filling in $(DEB_INST_DEV) using $(DEB_INST_DEV).in";\
		cat $(DEB_INST_DEV).in | $(call subst_vars) | \
			$(call merge_with, $(DEB_INST_DEV));\
	else \
		echo "Filling in $(DEB_INST_DEV) using $(DEB_INST_DEV_TEMPL)";\
		cat $(DEB_INST_DEV_TEMPL) | $(call subst_vars) | \
			$(call merge_with, $(DEB_INST_DEV));\
	fi

	$(H)install -d \
		debian/tmp/usr/lib/erlang/lib/$(E_PKG_NAME)-$(PKG_VERSION)/ebin
	$(H)install -m 644 ebin/* \
		debian/tmp/usr/lib/erlang/lib/$(E_PKG_NAME)-$(PKG_VERSION)/ebin/
	$(H)if [ -e include ]; then \
		install -d \
			debian/tmp/usr/lib/erlang/lib/$(E_PKG_NAME)-$(PKG_VERSION)/include ;\
		install -m 644 include/* \
			debian/tmp/usr/lib/erlang/lib/$(E_PKG_NAME)-$(PKG_VERSION)/include/;\
	fi
	$(H)if [ -e priv ]; then \
		install -d \
			debian/tmp/usr/lib/erlang/lib/$(E_PKG_NAME)-$(PKG_VERSION)/priv ;\
		install -m 644 priv/* \
			debian/tmp/usr/lib/erlang/lib/$(E_PKG_NAME)-$(PKG_VERSION)/priv/ ;\
		rm -rf debian/tmp/usr/lib/erlang/lib/$(E_PKG_NAME)-$(PKG_VERSION)/priv/Run-eunit-loop.expect ;\
	fi
