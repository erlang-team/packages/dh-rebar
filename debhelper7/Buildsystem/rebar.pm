# Copyright: © 2012 Enrico Tassi <gareuselesinge@debian.org>
# Copyright: 2013 Nobuhiro iwamatsu <iwamatsu@debian.org>
# License: MIT/X
# Strongly based on the ruby build systems. Thanks Lucas!

package Debian::Debhelper::Buildsystem::rebar;

use strict;
use base 'Debian::Debhelper::Buildsystem';

my $DH_REBAR_MAKEFILE="/usr/share/dh-rebar/make/dh-rebar.Makefile";

sub DESCRIPTION { "rebar" }

sub check_auto_buildable { return 0 }

sub new {
	my $class=shift;
	my $this=$class->SUPER::new(@_);
	$this->enforce_in_source_building();
	return $this;
}

sub configure {
	my $this=shift;
	$this->doit_in_sourcedir("make", "--no-print-directory", "-f", $DH_REBAR_MAKEFILE, "configure", @_);
}

sub build {
	my $this=shift;
	$this->doit_in_sourcedir("make", "--no-print-directory", "-f", $DH_REBAR_MAKEFILE, "build", @_);
}

sub test {
	my $this=shift;
	$this->doit_in_sourcedir("make", "--no-print-directory", "-f", $DH_REBAR_MAKEFILE, "test", @_);
}

sub install {
	my $this=shift;
	$this->doit_in_sourcedir("make", "--no-print-directory", "-f", $DH_REBAR_MAKEFILE, "install", @_);
}

sub clean {
	my $this=shift;
	$this->doit_in_sourcedir("make", "--no-print-directory", "-f", $DH_REBAR_MAKEFILE, "clean", @_);
}

1
